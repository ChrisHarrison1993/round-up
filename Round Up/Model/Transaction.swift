//
//  Transaction.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class Transaction: Object, Decodable {
    
    private enum TransactionCodingKeys: String, CodingKey {
        case id
        case currency
        case amount
        case direction
        case created
        case narrative
        case source
        case balance
    }
    
    dynamic var id: String = ""
    dynamic var currency: String = ""
    dynamic var amount: Double = 0.0
    dynamic var direction: String = ""
    dynamic var created: Date = Date()
    dynamic var narrative: String = ""
    dynamic var source: String = ""
    dynamic var balance: Double = 0.0 
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TransactionCodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.currency = try container.decode(String.self, forKey: .currency)
        self.amount = try container.decode(Double.self, forKey: .amount)
        self.direction = try container.decode(String.self, forKey: .direction)
        let createdString = try container.decode(String.self, forKey: .created)
        self.created = try DateConverter.toDate(dateString: createdString, format: .ISO8601)
        self.narrative = try container.decode(String.self, forKey: .narrative)
        self.source = try container.decode(String.self, forKey: .source)
        self.balance = try container.decode(Double.self, forKey: .balance)
        
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
