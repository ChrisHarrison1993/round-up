//
//  DataTaskResult.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation

class DataTaskResult<T: Decodable>: Decodable {
    
    private enum ResultCodingKeys: String, CodingKey {
        case _embedded
        case transactions
        case savingsGoalList
    }
    
    let _embedded: T?
    let transactions: T?
    let savingsGoalList: T?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResultCodingKeys.self)
        self._embedded = try? container.decode(T.self, forKey: ._embedded)
        self.transactions = try? container.decode(T.self, forKey: .transactions)
        self.savingsGoalList = try? container.decode(T.self, forKey: .savingsGoalList)
    }
}
