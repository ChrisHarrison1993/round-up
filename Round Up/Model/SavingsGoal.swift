//
//  SavingsGoal.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 30/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class SavingsGoal: Object, Decodable {
    
    private enum SavingsGoalCodingKeys: String, CodingKey {
        case id = "savingsGoalUid"
        case name
        case target
        case totalSaved
        case savedPercentage
    }
    
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var target: Double = 0.0
    dynamic var totalSaved: Double = 0.0
    dynamic var savedPercentage: Double = 0.0
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SavingsGoalCodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.target = try container.decode(CurrencyAndAmount.self, forKey: .target).minorUnits / 100
        self.totalSaved = try container.decode(CurrencyAndAmount.self, forKey: .totalSaved).minorUnits / 100
        self.savedPercentage = try container.decode(Double.self, forKey: .savedPercentage)
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}

@objcMembers class CurrencyAndAmount: Decodable {
    
    private enum CurrencyAndAmountCodingKeys: String, CodingKey {
        case currency
        case minorUnits
    }
    
    dynamic var currency: String = ""
    dynamic var minorUnits: Double = 0.0
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CurrencyAndAmountCodingKeys.self)
        self.currency = try container.decode(String.self, forKey: .currency)
        self.minorUnits = try container.decode(Double.self, forKey: .minorUnits)
    }
}
