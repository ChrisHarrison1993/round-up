//
//  TransactionType.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

enum TransactionType: String {
    case card = "MASTER_CARD"
    case paymentOut = "FASTER_PAYMENTS_OUT"
    case paymentIn = "FASTER_PAYMENTS_IN"
    case pot = "INTERNAL_TRANSFER"
    
    var image: UIImage? {
        switch self {
            case .card: return UIImage(named: "card")
            case .paymentOut: return UIImage(named: "paymentOut")
            case .paymentIn: return UIImage(named: "paymentIn")
            case .pot: return UIImage(named: "pot")
        }
    }
}
