//
//  NetworkService.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Alamofire

struct NetworkService {
    
    static let shared = NetworkService()
    
    private static let accessToken = "jFFGowLjULrbEE8PNFfxnLu7tEmdcKNTFSTl2oWxWhjUksmBvxR8X3BFhqmvugGy"
    private static let accountUid = "20a3dbf4-f120-88af-614c-171678eb2a8e"
    
    private static let baseURL = "https://api-sandbox.starlingbank.com"
    private static let transactionsPath = "/api/v1/transactions"
    private static let savingsGoalsPath = "/api/v2/account/\(accountUid)/savings-goals"
    private static let addMoneyPath = "/add-money"

    
    static var transations: String {
        return baseURL + transactionsPath
    }
    
    static var savingsGoals: String {
        return baseURL + savingsGoalsPath
    }
    
    func addSavingsGoal(with id: String) -> String {
        return NetworkService.baseURL + NetworkService.savingsGoalsPath + path(for: id) + NetworkService.addMoneyPath + path(for: UUID().uuidString)
    }
    
    func path(for path: String) -> String {
        return "/\(path)"
    }
    
    private static let headers: HTTPHeaders = [
        "Authorization": "Bearer \(accessToken)",
        "Accept": "application/json"
    ]
    
    func getTransactions(completion: @escaping (_ transactions: [Transaction]) -> ()) {
        Alamofire.request(NetworkService.transations, headers: NetworkService.headers).responseData { response in
            guard let json = response.result.value else { return }
            do {
                if let transactions = try JSONDecoder().decode(DataTaskResult<DataTaskResult<[Transaction]>>.self, from: json)._embedded?.transactions {
                    completion(transactions)
                }
            } catch {
                return
            }
        }
    }
    
    func getSavingsGoals(completion: @escaping (_ savingsGoals: [SavingsGoal]) -> ()) {
        Alamofire.request(NetworkService.savingsGoals, headers: NetworkService.headers).responseData { response in
            guard let json = response.result.value else { return }
            do {
                if let savingsGoals = try JSONDecoder().decode(DataTaskResult<[SavingsGoal]>.self, from: json).savingsGoalList {
                    completion(savingsGoals)
                }
            } catch {
                return
            }
        }
    }
    
    func createSavingsGoal(completion: @escaping (_ success: Bool) -> ()) {
        let parameters: Parameters = ["name": "Added to pot", "currency": "GBP", "target": ["currency": "GBP", "minorUnits": 10000]]
        Alamofire.request(NetworkService.savingsGoals, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: NetworkService.headers).responseData { response in
            completion(response.result.isSuccess)
        }
    }
    
    func transfer(amount: Double, to savingsGoal: String, completion: @escaping (_ success: Bool) -> ()) {
        let minorUnits = Int(amount)
        let parameters: Parameters = ["amount": ["currency": "GBP", "minorUnits": minorUnits]]
        Alamofire.request(addSavingsGoal(with: savingsGoal), method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: NetworkService.headers).responseData { response in
            completion(response.result.isSuccess)
        }
    }
}
