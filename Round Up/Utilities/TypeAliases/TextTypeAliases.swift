//
//  TextTypeAliases.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

typealias FontStyle = UIFont.TextStyle
typealias TextAlignment = NSTextAlignment
