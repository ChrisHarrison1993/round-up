//
//  DesignableButton.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 30/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

class PillButton: UIButton {
    
    override var intrinsicContentSize: CGSize {
        let baseSize = super.intrinsicContentSize
        return CGSize(width: baseSize.width + 40, height: baseSize.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLabel()
    }
    
    override func prepareForInterfaceBuilder() {
        setupLabel()
    }
    
    func setupLabel() {
        layer.cornerRadius = bounds.height/2
    }
}
