//
//  StyledText.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

struct StyledText {
    var text: String?
    var style: FontStyle
    var color: UIColor
    var alignment: TextAlignment
    
    init(text: String?, style: FontStyle, color: UIColor = .black, alignment: TextAlignment = .left ) {
        self.text = text
        self.style = style
        self.color = color
        self.alignment = alignment
    }
}
