//
//  CurrencyConverter.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 28/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation

enum CurrencyFormattingError: Error {
    case failedFormattingFromValue(_ value: NSNumber, locale: Locale)
}

class CurrencyFormatter {
    
    private static let shared = CurrencyFormatter()
    private let numberFormatter = NumberFormatter()
    
    static func toString(value: NSNumber, locale: Locale = Locale(identifier: "en_GB")) -> String? {
        let formatter = CurrencyFormatter.shared
        formatter.numberFormatter.locale = locale
        formatter.numberFormatter.numberStyle = .currency
        guard let currencyString = formatter.numberFormatter.string(from: value) else { return nil }
        return currencyString
    }
}
