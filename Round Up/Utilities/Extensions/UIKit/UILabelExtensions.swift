//
//  UILabelExtensions.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

extension UILabel {
    
    func style(with styledText: StyledText?) {
        guard let styledText = styledText else {
            self.text = nil
            return
        }
        self.text = styledText.text
        self.font = UIFont.preferredFont(forTextStyle: styledText.style)
        self.textColor = styledText.color
        self.textAlignment = styledText.alignment
    }
}
