//
//  DateExtensions.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 28/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation

extension Date {
    
    func oneWeekBefore() -> Date {
        //(back * seconds * minutes * hours * days)
        return self.addingTimeInterval(-1 * 60 * 60 * 24 * 7)
    }
}
