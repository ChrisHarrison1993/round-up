//
//  NSObjectExtensions.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        return "\(type(of: self))"
    }
    
    class var className: String {
        return "\(self)"
    }
}
