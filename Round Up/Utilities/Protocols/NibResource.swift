//
//  NibResource.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

protocol NibResource: class {
    static func nib() -> UINib?
}

extension NibResource where Self: NSObject {
    static func nib() -> UINib? {
        return UINib(nibName: self.className, bundle: nil)
    }
}

extension UITableViewCell: NibResource {
    
}
