//
//  DBManager.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    private var database: Realm
    static let shared = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func allTransactions() -> Results<Transaction> {
        return database.objects(Transaction.self).sorted(byKeyPath: "created", ascending: false)
    }
    
    func thisWeeksTransactions() -> Results<Transaction> {
        let oneWeekBeforeNow = Date().oneWeekBefore()
        let predicate = NSPredicate(format: "created > %@", oneWeekBeforeNow as NSDate)
        return allTransactions().filter(predicate)
    }
    
    func savingsGoals() -> Results<SavingsGoal> {
        return database.objects(SavingsGoal.self).sorted(byKeyPath: "id", ascending: true)
    }

    func save<T:Object>(_ items: [T]) {
        let itemList = List<T>()
        items.forEach({ itemList.append($0)})
        try! database.write {
            database.add(itemList, update: true)
        }
    }
}
