//
//  TransactionField.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

struct TransactionField {
    
    var image: UIImage?
    var description: StyledText
    var amount: StyledText
    var roundedAmount: StyledText
    var date: StyledText
    
    init(image: UIImage?, description: StyledText, amount: StyledText, roundedAmount: StyledText, date: StyledText) {
        self.image = image
        self.description = description
        self.amount = amount
        self.roundedAmount = roundedAmount
        self.date = date
    }
}
