//
//  TransactionCell.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell, TransactionUpdateable {
    
    @IBOutlet var typeImageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var roundedAmountLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    func update(with transactionField: TransactionField) {
        typeImageView.image = transactionField.image
        descriptionLabel.style(with: transactionField.description)
        amountLabel.style(with: transactionField.amount)
        roundedAmountLabel.style(with: transactionField.roundedAmount)
        dateLabel.style(with: transactionField.date)
    }
}
