//
//  TransactionsViewController.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit
import RealmSwift

class TransactionsViewController: UIViewController {
    
    @IBOutlet var dataProvider: TransactionsDataProvider!
    
    @IBOutlet var totalBalanceLabel: UILabel!
    @IBOutlet var totalRoundUpValueLabel: UILabel!
    @IBOutlet var potTotalLabel: UILabel!
    @IBOutlet var potView: DesignableView!
    @IBOutlet var createPotView: DesignableView!
    
    var savingsGoals: Results<SavingsGoal>?
    var savingsGoalsNotificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataProvider.delegate = self
        updateUI()
        fetchTransactions()
        fetchSavingsGoals()
    }
    
    func fetchTransactions() {
        NetworkService.shared.getTransactions { (transactions) in
            DBManager.shared.save(transactions)
        }
    }
    
    func fetchSavingsGoals() {
        NetworkService.shared.getSavingsGoals { (savingsGoals) in
            DBManager.shared.save(savingsGoals)
            self.observeSavingsGoals()
        }
    }
    
    func observeSavingsGoals() {
        savingsGoals = DBManager.shared.savingsGoals()
        savingsGoalsNotificationToken = savingsGoals?.observe{ (changes: RealmCollectionChange) in
            self.updateUI()
        }
    }
    
    func createSavingsGoal() {
        NetworkService.shared.createSavingsGoal { (success) in
            guard success else { return }
            self.fetchSavingsGoals()
        }
    }
    
    func transferRoundedAmountToSavingsGoal() {
        let transferAmount = (dataProvider.viewModel.roundUpTotal * 100).rounded()
        guard let savingsGoalID = savingsGoals?.first?.id else { return }
        NetworkService.shared.transfer(amount: transferAmount, to: savingsGoalID) { (success) in
            self.fetchSavingsGoals()
            self.fetchTransactions()
        }
    }
    
    @IBAction func createSavingsGoalPressed(_ sender: Any) {
        createSavingsGoal()
    }
    
    @IBAction func potButtonPressed(_ sender: Any) {
       transferRoundedAmountToSavingsGoal()
    }
    
    func hasPot() -> Bool {
        guard let savingsGoals = savingsGoals else {
            return false
        }
        return savingsGoals.count > 0
    }
}

extension TransactionsViewController: TransactionsDataProviderDelegate {
    
    func updateUI() {
        let balanceValue = CurrencyFormatter.toString(value: dataProvider.viewModel.currentBalance as NSNumber)
        let roundUpValue = CurrencyFormatter.toString(value: dataProvider.viewModel.roundUpTotal as NSNumber)
        let potTotalValue = CurrencyFormatter.toString(value: (savingsGoals?.first?.totalSaved ?? 0) as NSNumber)
        
        totalBalanceLabel.style(with: StyledText(text: balanceValue, style: .body))
        totalRoundUpValueLabel.style(with: StyledText(text: roundUpValue, style: .body))
        potTotalLabel.style(with: StyledText(text: potTotalValue, style: .body))
        
        potView.isHidden = !hasPot()
        createPotView.isHidden = hasPot()
    }
}
