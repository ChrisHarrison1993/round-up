//
//  TransactionsDataProvider.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import UIKit

protocol TransactionsDataProviderDelegate {
    func updateUI()
}

class TransactionsDataProvider: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            registerCells()
        }
    }
    
    var delegate: TransactionsDataProviderDelegate?
    
    var viewModel = TransactionsViewModel()
    
    override init() {
        super.init()
        viewModel.delegate = self
    }
    
    func registerCells() {
        tableView.register(TransactionCell.nib(), forCellReuseIdentifier: TransactionCell.className)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.item(at: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TransactionCell.className, for: indexPath) as? TransactionCell else {
            return UITableViewCell()
        }
        cell.update(with: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension TransactionsDataProvider: TransactionsViewModelDelegate {
    
    func items(insertedAtIndexPaths: [IndexPath], deletedAtIndexPaths: [IndexPath], modifiedAtIndexPaths: [IndexPath]) {
        tableView.beginUpdates()
        tableView.insertRows(at: insertedAtIndexPaths, with: .automatic)
        tableView.deleteRows(at: deletedAtIndexPaths, with: .automatic)
        tableView.reloadRows(at: modifiedAtIndexPaths, with: .automatic)
        tableView.endUpdates()
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    func updateUI() {
        delegate?.updateUI()
    }
}
