//
//  TransactionsViewModel.swift
//  Round Up
//
//  Created by Harrison, Chris (UK - London) on 27/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import RealmSwift

protocol TransactionsViewModelDelegate {
    func items(insertedAtIndexPaths: [IndexPath], deletedAtIndexPaths: [IndexPath], modifiedAtIndexPaths: [IndexPath])
    func reloadTableView()
    func updateUI()
}

class TransactionsViewModel {
    
    var transactionsNotificationToken: NotificationToken? = nil
    var transactionFields: [TransactionField] = []
    var transactions: Results<Transaction>!
    var roundUpTotal: Double = 0.0
    var currentBalance: Double = 0.0

    
    var delegate: TransactionsViewModelDelegate?
    
    init() {
        observeTransactions()
        updateFields()
        self.reload()
    }
    
    func numberOfRows(in section: Int) -> Int {
        return transactionFields.count
    }
    
    func item(at indexPath: IndexPath) -> TransactionField {
        return transactionFields[indexPath.row]
    }
    
    func observeTransactions() {
        transactions = DBManager.shared.thisWeeksTransactions()
        transactionsNotificationToken = transactions.observe { (changes: RealmCollectionChange) in
            self.updateFields()
            self.delegate?.updateUI()
            switch changes {
            case .initial(_):
                self.reload()
            case .update(_, let deletions, let insertions, let modifications):
                self.delegate?.items(insertedAtIndexPaths: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     deletedAtIndexPaths: deletions.map({ IndexPath(row: $0, section: 0) }),
                                     modifiedAtIndexPaths: modifications.map({ IndexPath(row: $0, section: 0) }))
            case .error(_):
                return
            }
        }
    }
    
    func updateFields() {
        currentBalance = transactions.first?.balance ?? 0.0
        var newRoundUpTotal: Double = 0.0
        
        transactionFields = transactions.map({ (transaction)  in
            var amount: String?
            var roundAmount: String?
            if transaction.amount < 0 && TransactionType(rawValue: transaction.source) != .pot {
                let roundAmountValue = roundedValue(for: transaction.amount)
                newRoundUpTotal += roundAmountValue
                roundAmount = "+\(CurrencyFormatter.toString(value: roundAmountValue as NSNumber) ?? "")"
                amount = CurrencyFormatter.toString(value: transaction.amount.rounded(.up) as NSNumber)
            } else {
                amount = CurrencyFormatter.toString(value: transaction.amount as NSNumber)
            }
            roundUpTotal = newRoundUpTotal
            
            let dateString = DateConverter.toString(date: transaction.created, format: .EEEE_d_MMMM)
            
            return TransactionField(
                image: TransactionType(rawValue: transaction.source)?.image,
                description: StyledText(text: transaction.narrative, style: .body),
                amount: StyledText(text: amount, style: .body, alignment: .right),
                roundedAmount: StyledText(text: roundAmount, style: .caption1, color: .lightGray, alignment: .right),
                date: StyledText(text: dateString, style: .caption1, color: .lightGray)
            )
        })
    }
    
    func reload() {
        self.delegate?.reloadTableView()
    }
    
    func roundedValue(for amount: Double) -> Double {
        let remainder: Double = Double(Int((abs(amount) * 100)))
        if remainder == 0 {
            return remainder
        } else {
            return 1 - (remainder / 10000)
        }
    }
}
