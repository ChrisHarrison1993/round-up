//
//  NSObjectExtensionsTests.swift
//  Round UpTests
//
//  Created by Harrison, Chris (UK - London) on 30/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import XCTest

@testable import Round_Up
class NSObjectExtensionsTests: XCTestCase {
    
    func test_ClassNameFromTransactionCell() {
        let transactionCell = TransactionCell()
        XCTAssertEqual(transactionCell.className, "TransactionCell")
    }
    
    func test_ClassNameFromSelf() {
        XCTAssertEqual(self.className, "NSObjectExtensionsTests")
    }
    
}

