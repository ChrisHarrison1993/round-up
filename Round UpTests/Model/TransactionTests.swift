//
//  TransactionTests.swift
//  Round UpTests
//
//  Created by Harrison, Chris (UK - London) on 30/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import XCTest

@testable import Round_Up
class TransactionTests: XCTestCase {
    
    var transactions: [Transaction]!
    
    var testTransactions: Data!
    
    override func setUp() {
        let testTransactionBundle = Bundle(for: type(of: self))
        let mockJSONURL = testTransactionBundle.url(forResource: "testTransactions", withExtension: "json")!
        let testTransactions = try! Data(contentsOf: mockJSONURL)
        let transactions = try! JSONDecoder().decode(DataTaskResult<DataTaskResult<[Transaction]>>.self, from: testTransactions)._embedded?.transactions
        DBManager.shared.save(transactions!)
    }
    
    func test_TransactionHasPrimaryKey() {
        XCTAssertNotNil(Transaction.primaryKey())
    }
}
