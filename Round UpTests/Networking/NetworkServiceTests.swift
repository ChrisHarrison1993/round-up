//
//  NetworkServiceTests.swift
//  Round UpTests
//
//  Created by Harrison, Chris (UK - London) on 30/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import XCTest

@testable import Round_Up
class NetworkServiceTests: XCTestCase {
    
    func test_transactionArrayReturnedFromRequest() {
        var testTransactions: [Transaction]!
        let expectation = self.expectation(description: "Transaction")
        NetworkService.shared.getTransactions { (transactions) in
            testTransactions = transactions
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertNotNil(testTransactions)
    }
    
    func test_savingsGoalsArrayReturnedFromRequest() {
        var testSavingsGoals: [SavingsGoal]!
        let expectation = self.expectation(description: "SavingsGoal")
        NetworkService.shared.getSavingsGoals { (savingsGoals) in
            testSavingsGoals = savingsGoals
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertNotNil(testSavingsGoals)
    }
    
    func test_createSavingsGoalSuccessReturnedFromRequest() {
        var testCreateSavingsGoal: Bool!
        let expectation = self.expectation(description: "CreateSavingsGoal")
        NetworkService.shared.createSavingsGoal { (success) in
            testCreateSavingsGoal = success
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertNotNil(testCreateSavingsGoal)
    }
}
