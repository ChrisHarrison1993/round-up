//
//  TransactionsViewControllerTests.swift
//  Round UpTests
//
//  Created by Harrison, Chris (UK - London) on 31/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import XCTest

@testable import Round_Up
class TransactionsViewControllerTests: XCTestCase {
    
    var vc: TransactionsViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Transactions", bundle: nil)
        vc = (storyboard.instantiateInitialViewController() as! TransactionsViewController)
        _ = vc.view
    }

    func test_HasDataProvider() {
        XCTAssertNotNil(vc.dataProvider)
    }
}
