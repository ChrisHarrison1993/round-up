//
//  TransactionsDataProviderTests.swift
//  Round UpTests
//
//  Created by Harrison, Chris (UK - London) on 31/03/2019.
//  Copyright © 2019 Harrison, Chris (UK - London). All rights reserved.
//

import XCTest

@testable import Round_Up
class TransactionsDataProviderTests: XCTestCase {
    
    var vc: TransactionsViewController!
    var dataProvider: TransactionsDataProvider!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Transactions", bundle: nil)
        vc = (storyboard.instantiateInitialViewController() as! TransactionsViewController)
        _ = vc.view
        dataProvider = vc.dataProvider
    }
    
    func test_DataProviderHasTableView() {
        XCTAssertNotNil(dataProvider.tableView)
    }
    
    func test_DataProviderHasViewModel() {
        XCTAssertNotNil(dataProvider.viewModel)
    }
    
    func test_TableViewHasOneSection() {
        XCTAssert(dataProvider.tableView.numberOfSections == 1)
    }
}
